package com.chon.microservice.customer.api;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/customer")
public class CustomerContoller {


    @GetMapping("/findAll")
    public List<Customer> findAllCustomer() {

        log.info("customer WAS CALLED");

        List<Customer> simList = new ArrayList<Customer>();

        Customer simData = new Customer();
        simData.setCustomerId("12");
        simData.setName(RandomStringUtils.randomAlphabetic(10));

        simList.add(simData);


        simData = new Customer();
        simData.setCustomerId("1");
        simData.setName(RandomStringUtils.randomAlphabetic(10));

        simList.add(simData);

        return simList;
    }

    @GetMapping("/{id}")
    public Customer findCustomer(@PathVariable String id) {

        log.info("customer WAS CALLED");

        Customer simData = new Customer();
        simData.setCustomerId(id);
        simData.setName(RandomStringUtils.randomAlphabetic(10));

        return simData;
    }

}
