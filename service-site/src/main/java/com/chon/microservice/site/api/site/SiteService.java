package com.chon.microservice.site.api.site;

import com.chon.microservice.site.api.customer.Customer;
import com.chon.microservice.site.api.customer.CustomerClient;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SiteService {

    @Autowired
    private CustomerClient customerClient;

    public List<Site> findAllSite(){


        List<Site> simList = new ArrayList<>();

        Site simData;
        for (int i = 0; i < 100 ; i++) {
            simData = new Site();
            simData.setSiteId(String.valueOf(i));
            simData.setName(RandomStringUtils.randomAlphabetic(10));
            simData.setCustomerId(String.valueOf(i));

            simList.add(simData);
        }

        Optional<List<Site>> sites = Optional.ofNullable(simList);

        sites.ifPresent(d -> {
            for (Site item:d) {
                if(null != item) {
                    Customer customer = customerClient.findCustomerById(item.getCustomerId());
                    item.setCustomer(customer);
                }
            }
        });

        return sites.orElse(null);
    }

    public Site findSiteById(String id){
        Site simData = new Site();
        simData.setSiteId(id);
        simData.setName(RandomStringUtils.randomAlphabetic(10));
        simData.setCustomerId("10");

        Optional<Site> site = Optional.ofNullable(simData);

        site.ifPresent(d -> {
            Customer customer = customerClient.findCustomerById(d.getCustomerId());
            d.setCustomer(customer);
        });


        return site.orElse(null);
    }

}
