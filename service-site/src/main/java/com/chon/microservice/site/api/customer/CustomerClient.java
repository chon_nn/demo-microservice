package com.chon.microservice.site.api.customer;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "service-customer")
public interface CustomerClient {

    @GetMapping("/customer/{id}")
    Customer findCustomerById(@PathVariable String id);

}
