package com.chon.microservice.site.api.site;

import com.chon.microservice.site.api.customer.Customer;
import com.chon.microservice.site.api.customer.CustomerClient;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/site")
public class SiteController {

    @Autowired
    private CustomerClient customerClient;

    @Autowired
    private SiteService siteService;

    @GetMapping("/findAll")
    public List<Site> findAllSite() {
        return siteService.findAllSite();
    }

    @GetMapping("/{id}")
    public Site findCustomer(@PathVariable String id) {
        return siteService.findSiteById(id);
    }

}
